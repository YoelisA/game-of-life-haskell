module GameOfLife (tick) where

import qualified Data.List as List
import qualified Data.Map as Map
import Data.Maybe (catMaybes)

tick :: [[Int]] -> [[Int]]
tick prev =
  let matrixWithCol = zip [1 ..] prev
      matrixWithCoordinates = map (\(y, row) -> zipWith (\x cell -> ((x, y), cell)) [1 ..] row) matrixWithCol
      matrixAsDict :: Map.Map (Int, Int) Int
      matrixAsDict = Map.fromList (concat matrixWithCoordinates)
   in case prev of
        [] -> []
        otherwise ->
          map
            (map (\(pos, cell) -> cell))
            ( List.groupBy
                (\((x, y), _) ((x', y'), _) -> y' > y)
                (Map.toList (Map.mapWithKey (\key cell -> cellUpdate key cell matrixAsDict) matrixAsDict))
            )

cellUpdate :: (Int, Int) -> Int -> Map.Map (Int, Int) Int -> Int
cellUpdate (row, col) cell matrix =
  let (left, right) = (Map.lookup ((row - 1), col) matrix, Map.lookup ((row + 1), col) matrix)
      (top, down) = (Map.lookup (row, (col + 1)) matrix, Map.lookup (row, (col - 1)) matrix)
      (topLeft, downLeft) = (Map.lookup ((row - 1), (col + 1)) matrix, Map.lookup ((row - 1), (row - 1)) matrix)
      (topRight, downRight) = (Map.lookup ((row + 1), (col + 1)) matrix, Map.lookup ((row + 1), (col - 1)) matrix)
      neighbors = catMaybes ([left, right, top, down, topLeft, downLeft, topRight, downRight])
      aliveNeighbors :: Int
      aliveNeighbors = (length . filter ((==) 1)) neighbors
   in case cell of
        1 | aliveNeighbors == 2 || aliveNeighbors == 3 -> 1
        0 | aliveNeighbors == 3 -> 1
        _ -> 0
